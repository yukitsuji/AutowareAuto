// Copyright 2018 Apex.AI, Inc.
// Co-developed by Tier IV, Inc. and Apex.AI, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "pure_pursuit/pure_pursuit.hpp"

namespace autoware
{
namespace planning
{
namespace waypoint_follower
{
namespace pure_pursuit
{

PurePursuit::PurePursuit(
  double minimum_lookahead_distance,
  double lookahead_distance_ratio)
: m_min_lookahead_distance(minimum_lookahead_distance),
  m_lookahead_distance_ratio(lookahead_distance_ratio),
  m_lookahead_distance(0),
  // m_current_waypoints()
  m_current_velocity(0)
  // m_current_pose(rosidl_generator_cpp::MessageInitialization::ALL)
{
}

// PurePursuit::PurePursuit(){};
void PurePursuit::set_current_waypoints(
  const std::vector<autoware_msgs::msg::Waypoint> &waypoints)
{
  m_current_waypoints = waypoints;
}

void PurePursuit::set_current_velocity(const double velocity)
{
  m_current_velocity = velocity;
}
void PurePursuit::set_current_pose(const geometry_msgs::msg::Pose & pose)
{
  m_current_pose = pose;
}

bool PurePursuit::compute_curvature(double *kappa)
{
  compute_lookahead_distance();
  const bool is_next_waypoint = get_next_waypoint();
  if (!is_next_waypoint) {
    return false;
  }

  m_next_target_position = m_current_waypoints[m_next_waypoint_index].pose.pose.position;
  *kappa = compute_kappa();

  return true;
}

void PurePursuit::compute_lookahead_distance()
{
  const double max_lookahead_distance = m_current_velocity * 10;
  const double pred_lookahead_distance = m_current_velocity * m_lookahead_distance_ratio;
  m_lookahead_distance = pred_lookahead_distance < m_min_lookahead_distance ? m_min_lookahead_distance :
    pred_lookahead_distance < max_lookahead_distance ? pred_lookahead_distance : max_lookahead_distance;
}

double PurePursuit::get_plane_distance(geometry_msgs::msg::Point pos1,
                          geometry_msgs::msg::Point pos2) const
{
  return sqrt(pow(pos1.x - pos2.x, 2) + pow(pos1.y - pos2.y, 2) + pow(pos1.z - pos2.z, 2));
}

bool PurePursuit::get_next_waypoint()
{
  const uint32_t point_size = static_cast<uint32_t>(m_current_waypoints.size());
  if (point_size == 0) {
    return false;
  }
  for (uint32_t i = 0; i < point_size; ++i) {
    if (get_plane_distance(m_current_waypoints[i].pose.pose.position,
                           m_current_pose.position) > m_lookahead_distance) {
      m_next_waypoint_index = i;
      return true;
    }
  }
  std::cout << "Search waypoint is the last\n";
  m_next_waypoint_index = point_size - 1;
  return true;
}

geometry_msgs::msg::Point PurePursuit::calc_relative_coordinate(const geometry_msgs::msg::Point point)
{
  tf2::Vector3 vec(m_current_pose.position.x, m_current_pose.position.y, m_current_pose.position.z);
  tf2::Quaternion q(m_current_pose.orientation.x, m_current_pose.orientation.y,
                  m_current_pose.orientation.z, m_current_pose.orientation.w);

  tf2::Transform inverse(q, vec);
  tf2::Transform transform = inverse.inverse();
  tf2::Vector3 tf_point(point.x, point.y, point.z);
  tf_point = transform * tf_point;
  geometry_msgs::msg::Point tf_point_msg;
  tf_point_msg.x = tf_point.m_floats[0];
  tf_point_msg.y = tf_point.m_floats[1];
  tf_point_msg.z = tf_point.m_floats[2];
  return tf_point_msg;
}

double PurePursuit::compute_kappa()
{
  const double denominator = pow(get_plane_distance(m_next_target_position,
                                   m_current_pose.position), 2);
  const double numerator = 2 * calc_relative_coordinate(m_next_target_position).y;
  constexpr double KAPPA_MIN = 1 / 9e10;
  if (denominator != 0) {
    return numerator / denominator;
  } else if (numerator > 0) {
    return KAPPA_MIN;
  } else {
    return KAPPA_MIN;
  }
}

}  // namespace pure_pursuit
}  // namespace waypoint_follower
}  // namespace planning
}  // namespace autoware
