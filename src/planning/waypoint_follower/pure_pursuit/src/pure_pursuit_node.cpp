// Copyright 2018 Apex.AI, Inc.
// Co-developed by Tier IV, Inc. and Apex.AI, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "pure_pursuit/pure_pursuit_node.hpp"

//lint -e537 NOLINT  // cpplint vs pclint
#include <string>

namespace autoware
{
namespace planning
{
namespace waypoint_follower
{
namespace pure_pursuit
{

PurePursuitNode::PurePursuitNode(
  const std::string & node_name,
  const std::string & waypoints_sub_topic,
  const std::string & pose_sub_topic,
  const std::string & twist_sub_topic,
  const std::string & twist_pub_topic,
  const std::string & next_waypoint_pub_topic,
  const std::string & target_mark_pub_topic,
  const std::chrono::milliseconds & loop_rate,
  const double minimum_lookahead_distance,
  const double lookahead_distance_ratio)
:  Node(node_name),
   m_is_waypoint_set(false),
   m_is_velocity_set(false),
   m_is_pose_set(false),
   m_linear_velocity(0),
   m_pure_pursuit(
     PurePursuit(minimum_lookahead_distance,
                 lookahead_distance_ratio)),
   m_waypoint_sub_ptr(
     this->create_subscription<Lane>(
       waypoints_sub_topic,
       std::bind(&PurePursuitNode::waypoint_callback, this, std::placeholders::_1))),
   m_pose_sub_ptr(
     this->create_subscription<PoseStamped>(
       pose_sub_topic,
       std::bind(&PurePursuitNode::pose_callback, this, std::placeholders::_1))),
   m_twist_sub_ptr(
     this->create_subscription<TwistStamped>(
       twist_sub_topic,
       std::bind(&PurePursuitNode::twist_callback, this, std::placeholders::_1))),
   m_twist_pub_ptr(
     this->create_publisher<TwistStamped>(
       twist_pub_topic)),
   m_next_waypoint_pub_ptr(
     this->create_publisher<Marker>(
       next_waypoint_pub_topic)),
   m_target_mark_pub_ptr(
     this->create_publisher<Marker>(
       target_mark_pub_topic)),
   m_timer_ptr(
     this->create_wall_timer(
       std::chrono::duration_cast<std::chrono::nanoseconds>(loop_rate),
       std::bind(&PurePursuitNode::main_callback, this)))
{
}

PurePursuitNode::~PurePursuitNode()
{
}

void PurePursuitNode::waypoint_callback(const Lane::SharedPtr msg_ptr)
{
  if (!msg_ptr->waypoints.empty()) {
    m_linear_velocity = msg_ptr->waypoints[0].twist.twist.linear.x;
  } else {
    m_linear_velocity = 0;
  }

  m_pure_pursuit.set_current_waypoints(msg_ptr->waypoints);
  m_is_waypoint_set = true;
}

void PurePursuitNode::pose_callback(const PoseStamped::SharedPtr msg_ptr)
{
  m_pure_pursuit.set_current_pose(msg_ptr->pose);
  m_is_pose_set = true;
}

void PurePursuitNode::twist_callback(const TwistStamped::SharedPtr msg_ptr)
{
  m_pure_pursuit.set_current_velocity(msg_ptr->twist.linear.x);
  m_is_velocity_set = true;
}

void PurePursuitNode::main_callback()
{
  if (!m_is_waypoint_set || !m_is_velocity_set || !m_is_pose_set) {
    std::cout << "Necessary topics are not subscribed\n";
    return;
  }

  double kappa = 0;
  const bool can_get_curvature = m_pure_pursuit.compute_curvature(&kappa);
  std::cout << "can_get_curvature " << can_get_curvature << "\n";

  geometry_msgs::msg::TwistStamped twist;
  twist.header.stamp = now();
  twist.twist.linear.x = m_linear_velocity;
  twist.twist.angular.z = can_get_curvature ? kappa * twist.twist.linear.x : 0;
  m_twist_pub_ptr->publish(twist);

  m_is_waypoint_set = false;
  m_is_velocity_set = false;
  m_is_pose_set = false;
}


}  // namespace pure_pursuit
}  // namespace waypoint_follower
}  // namespace planning
}  // namespace autoware
