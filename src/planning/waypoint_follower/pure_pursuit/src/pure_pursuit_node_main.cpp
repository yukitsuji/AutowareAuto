// Copyright 2018 Apex.AI, Inc.
// Co-developed by Tier IV, Inc. and Apex.AI, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// \copyright Copyright 2018 Apex.AI, Inc.
/// \file
/// \brief main function for pure_pursuit

#include "pure_pursuit/pure_pursuit_node.hpp"
#include "boost/program_options.hpp"

int32_t main(const int32_t argc, char ** const argv)
{
  int32_t ret;
  try {
    rclcpp::init(argc, argv);

    boost::program_options::options_description desc("Allowed options");
    desc.add_options()("help", "produce help message")("node_name",
      boost::program_options::value<std::string>()->
      default_value("pure_pursuit_node"),
      "Name of this node")("waypoints_sub_topic",
      boost::program_options::value<std::string>()->
      default_value("final_waypoints"),
      "Name of the topic name of input wayponits")("pose_sub_topic",
      boost::program_options::value<std::string>()->
      default_value("current_pose"),
      "Namespace of the topic name of an input pose")("twist_sub_topic",
      boost::program_options::value<std::string>()->
      default_value("current_velocity"),
      "Namespace of the topic name of an input twist")("twist_pub_topic",
      boost::program_options::value<std::string>()->
      default_value("twist_raw"),
      "Namespace of the topic name of an input twist")("next_waypoint_pub_topic",
      boost::program_options::value<std::string>()->
      default_value("next_waypoint_mark"),
      "Namespace of the topic name of an output next_waypoint")("target_mark_pub_topic",
      boost::program_options::value<std::string>()->
      default_value("next_target_mark"),
      "Namespace of the topic name of an output target_mark")("loop_rate",
      boost::program_options::value<int>()->
      default_value(30),
      "Loop rate(ms)")("minimum_lookahead_distance",
      boost::program_options::value<double>()->
      default_value(5.0),
      "minimum_lookahead_distance")("lookahead_distance_ratio",
      boost::program_options::value<double>()->
      default_value(2.0),
      "lookahead_distance_ratio");
    boost::program_options::variables_map vm;
    boost::program_options::store(
      boost::program_options::parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);

    std::string node_name = vm["node_name"].as<std::string>().c_str();
    std::string waypoints_sub_topic = vm["waypoints_sub_topic"].as<std::string>().c_str();
    std::string pose_sub_topic = vm["pose_sub_topic"].as<std::string>().c_str();
    std::string twist_sub_topic = vm["twist_sub_topic"].as<std::string>().c_str();
    std::string twist_pub_topic = vm["twist_pub_topic"].as<std::string>().c_str();
    std::string next_waypoint_pub_topic = vm["next_waypoint_pub_topic"].as<std::string>().c_str();
    std::string target_mark_pub_topic = vm["target_mark_pub_topic"].as<std::string>().c_str();
    std::chrono::milliseconds loop_rate(vm["loop_rate"].as<int>());
    double minimum_lookahead_distance(vm["minimum_lookahead_distance"].as<double>());
    double lookahead_distance_ratio(vm["lookahead_distance_ratio"].as<double>());

    using autoware::planning::waypoint_follower::pure_pursuit::PurePursuitNode;
    const std::shared_ptr<PurePursuitNode> node_ptr =
      std::make_shared<PurePursuitNode>(
        node_name,
        waypoints_sub_topic,
        pose_sub_topic,
        twist_sub_topic,
        twist_pub_topic,
        next_waypoint_pub_topic,
        target_mark_pub_topic,
        loop_rate,
        minimum_lookahead_distance,
        lookahead_distance_ratio);

    rclcpp::executors::SingleThreadedExecutor exec;
    exec.add_node(node_ptr);

    exec.spin();

    rclcpp::shutdown();
    ret = 0;
  } catch (const std::exception & e) {
    std::cerr << e.what() << "\n";
    ret = 1;
  } catch (...) {
    std::cerr << "Unknown error occured" << "\n";
    ret = 255;
  }

  return ret;
}
