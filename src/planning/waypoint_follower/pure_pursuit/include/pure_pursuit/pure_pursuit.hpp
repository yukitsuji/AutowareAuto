// Copyright 2018 Apex.AI, Inc.
// Co-developed by Tier IV, Inc. and Apex.AI, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// \copyright Copyright 2018 Apex.AI, Inc.
/// \file
/// \brief This file defines the pure_pursuit class.

#ifndef PURE_PURSUIT__PURE_PURSUIT_HPP_
#define PURE_PURSUIT__PURE_PURSUIT_HPP_

#include <pure_pursuit/visibility_control.hpp>
#include <iostream>
#include <autoware_msgs/msg/lane.hpp>
#include <autoware_msgs/msg/waypoint.hpp>
#include <geometry_msgs/msg/pose_stamped.hpp>
#include <geometry_msgs/msg/twist_stamped.hpp>
#include <visualization_msgs/msg/marker.hpp>
#include <tf2/LinearMath/Transform.h>

namespace autoware
{
namespace planning
{
namespace waypoint_follower
{
/// \brief TODO(yukitsuji020832): Document namespaces!
namespace pure_pursuit
{

class PurePursuit
{
public:
  PurePursuit(
    double minimum_lookahead_distance,
    double lookahead_distance_ratio);

  void set_current_waypoints(const std::vector<autoware_msgs::msg::Waypoint> & waypoints);
  void set_current_velocity(const double velocity);
  void set_current_pose(const geometry_msgs::msg::Pose & pose);
  bool compute_curvature(double *kappa);
private:
  void compute_lookahead_distance();
  bool get_next_waypoint();
  double get_plane_distance(geometry_msgs::msg::Point pos1,
                            geometry_msgs::msg::Point pos2) const;
  geometry_msgs::msg::Point calc_relative_coordinate(const geometry_msgs::msg::Point point);
  double compute_kappa();

  double m_min_lookahead_distance;
  double m_lookahead_distance_ratio;
  double m_lookahead_distance;
  uint32_t m_next_waypoint_index;
  geometry_msgs::msg::Point m_next_target_position;
  std::vector<autoware_msgs::msg::Waypoint> m_current_waypoints;
  double m_current_velocity;
  geometry_msgs::msg::Pose m_current_pose;
};

}  // namespace pure_pursuit
}  // namespace waypoint_follower
}  // namespace planning
}  // namespace autoware

#endif  // PURE_PURSUIT__PURE_PURSUIT_HPP_
