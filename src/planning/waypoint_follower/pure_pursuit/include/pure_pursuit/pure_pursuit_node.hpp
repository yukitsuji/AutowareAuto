// Copyright 2018 Apex.AI, Inc.
// Co-developed by Tier IV, Inc. and Apex.AI, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// \copyright Copyright 2018 Apex.AI, Inc.
/// \file
/// \brief This file defines the pure_pursuit_node class.

#ifndef PURE_PURSUIT__PURE_PURSUIT_NODE_HPP_
#define PURE_PURSUIT__PURE_PURSUIT_NODE_HPP_

#include <pure_pursuit/pure_pursuit.hpp>
#include <rclcpp/rclcpp.hpp>
#include <chrono>
#include <string>
#include <autoware_msgs/msg/lane.hpp>
#include <autoware_msgs/msg/waypoint.hpp>
#include <geometry_msgs/msg/pose_stamped.hpp>
#include <geometry_msgs/msg/twist_stamped.hpp>
#include <visualization_msgs/msg/marker.hpp>

namespace autoware
{
namespace planning
{
namespace waypoint_follower
{
namespace pure_pursuit
{

using autoware_msgs::msg::Lane;
using geometry_msgs::msg::PoseStamped;
using geometry_msgs::msg::TwistStamped;
using visualization_msgs::msg::Marker;

/// \class PurePursuitNode
/// \brief ROS 2 Node for hello world.
class PurePursuitNode : public rclcpp::Node
{
public:
  /// \brief default constructor, starts driver
  /// \param[in] node_name name of the node for rclcpp internals
  /// \throw runtime error if failed to start threads or configure driver
  explicit PurePursuitNode(
    const std::string & node_name,
    const std::string & waypoints_sub_topic,
    const std::string & pose_sub_topic,
    const std::string & twist_sub_topic,
    const std::string & twist_pub_topic,
    const std::string & next_waypoint_pub_topic,
    const std::string & target_mark_pub_topic,
    const std::chrono::milliseconds & loop_rate,
    const double minimum_lookahead_distance,
    const double lookahead_distance_ratio);

  /// \brief default destructor
  virtual ~PurePursuitNode();

private:
  void waypoint_callback(const Lane::SharedPtr msg_ptr);
  void pose_callback(const PoseStamped::SharedPtr msg_ptr);
  void twist_callback(const TwistStamped::SharedPtr msg_ptr);
  void main_callback();

  bool m_is_waypoint_set;
  bool m_is_velocity_set;
  bool m_is_pose_set;
  double m_linear_velocity;

  PurePursuit m_pure_pursuit;

  const rclcpp::Subscription<Lane>::SharedPtr m_waypoint_sub_ptr;
  const rclcpp::Subscription<PoseStamped>::SharedPtr m_pose_sub_ptr;
  const rclcpp::Subscription<TwistStamped>::SharedPtr m_twist_sub_ptr;
  const rclcpp::Publisher<TwistStamped>::SharedPtr m_twist_pub_ptr;
  const rclcpp::Publisher<Marker>::SharedPtr m_next_waypoint_pub_ptr;
  const rclcpp::Publisher<Marker>::SharedPtr m_target_mark_pub_ptr;
  const rclcpp::TimerBase::SharedPtr m_timer_ptr;
};
}  // namespace pure_pursuit
}  // namespace waypoint_follower
}  // namespace planning
}  // namespace autoware

#endif  // PURE_PURSUIT__PURE_PURSUIT_NODE_HPP_
